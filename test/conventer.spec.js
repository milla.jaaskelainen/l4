const expect = require("chai").expect
const conventer = require("../src/conventer")

describe("Color code conventer", ()=>{
    describe("RGB to Hex conversion", ()=>{
        it("converts the basic colors", ()=>{
            const redHex = conventer.rgbToHex(255, 0, 0)
            const greenHex = conventer.rgbToHex(0, 255, 0)
            const blueHex = conventer.rgbToHex(0, 0, 255)

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })
    })
    describe("Hex to RGB conversion", ()=>{
        it("converts the basic colors", ()=>{
            const redRgb = conventer.hexToRbg("ff0000")
            const greenRgb = conventer.hexToRbg("00ff00")
            const blueRgb = conventer.hexToRbg("0000ff")

            expect(redRgb).to.equal("255,0,0")
            expect(greenRgb).to.equal("0,255,0")
            expect(blueRgb).to.equal("0,0,255")
        })
    })
})